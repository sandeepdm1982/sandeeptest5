package net.viralpatel.spring.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.mysql.jdbc.Statement;

import net.viralpatel.spring.model.Customer;
import net.viralpatel.spring.rowmapper.CustomerRowMapper;

/**
 * https://www.mkyong.com/spring/spring-jdbctemplate-querying-examples/
 * @author sande
 *
 */
@Component
public class CustomerDAO2 {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public void addPerson(final Customer customerObj) {
		final String SQL = "INSERT INTO customer (firstName, lastName, email, mobile,dateOfBirth) "
				+ " VALUES(?,?,?,?,?)";

		KeyHolder holder = new GeneratedKeyHolder();

		jdbcTemplate.update(new PreparedStatementCreator() {

			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(SQL.toString(), Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, customerObj.getFirstName());
				ps.setString(2, customerObj.getLastName());
				ps.setString(3, customerObj.getEmail());
				ps.setString(4, customerObj.getMobile());
				ps.setDate(5, customerObj.getDateOfBirth());
				return ps;
			}
		}, holder);
		System.out.println("Customer Added --->" + holder.getKey());
		customerObj.setId(holder.getKey().longValue());
	}

	public void updatePerson(final Customer customerObj, final long customerId) {
		final String SQL = "Update customer set firstName=?,lastName=?,email=?,mobile=?,dateOfBirth=? where id =? ";

		KeyHolder holder = new GeneratedKeyHolder();

		jdbcTemplate.update(new PreparedStatementCreator() {

			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(SQL.toString(), Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, customerObj.getFirstName());
				ps.setString(2, customerObj.getLastName());
				ps.setString(3, customerObj.getEmail());
				ps.setString(4, customerObj.getMobile());
				ps.setDate(5, customerObj.getDateOfBirth());
				ps.setLong(6, customerId);
				return ps;
			}
		});
		System.out.println("Customer Update --->" + holder.getKey());
	}

	public int deletePerson(final long customerId) {
		final String SQL = "delete from customer where id ='" + customerId + "'";
		int result = jdbcTemplate.update(SQL);
		return result;
	}

	public List<Customer> getAllCustomers() {
		String sql = "SELECT * FROM CUSTOMER";

		List<Customer> customersArrayListObj = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Customer.class));
		return customersArrayListObj;
	}

	public Customer getCustomer(long customerId) {
		Customer customerObj = null;
		
		try{
		String sql = "SELECT * FROM CUSTOMER where id= ?";
		SqlParameterSource namedParameters = new MapSqlParameterSource("id", customerId);

		customerObj = (Customer) jdbcTemplate.queryForObject(sql, new Object[] { customerId },
				new CustomerRowMapper());
		return customerObj;
		}catch (final EmptyResultDataAccessException e) {
			return null;
		}
		
	}
}