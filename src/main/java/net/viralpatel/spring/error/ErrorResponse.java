package net.viralpatel.spring.error;

import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * https://lankydanblog.com/2017/09/12/global-exception-handling-with-controlleradvice/
 * https://dzone.com/articles/global-exception-handling-with-controlleradvice
 * 
 * @author sande
 *
 */

public class ErrorResponse extends ResponseEntityExceptionHandler {
	private Throwable exceptionObj;
	
	public ErrorResponse(Throwable e){
		this.exceptionObj = e;
	}

	public Throwable getExceptionObj() {
		return exceptionObj;
	}

	public void setExceptionObj(Throwable exceptionObj) {
		this.exceptionObj = exceptionObj;
	}
	
}
