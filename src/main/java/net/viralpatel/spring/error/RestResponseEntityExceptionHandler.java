package net.viralpatel.spring.error;

import java.util.Optional;

import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import net.viralpatel.spring.controller.CustomerRestController;

/**
 * https://lankydanblog.com/2017/09/12/global-exception-handling-with-controlleradvice/
 * https://dzone.com/articles/global-exception-handling-with-controlleradvice
 * 
 * @author sande
 *
 */
@ControllerAdvice(basePackageClasses = CustomerRestController.class)
@RequestMapping(produces = "application/vnd.error+json")
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	/*@ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
	protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
		String bodyOfResponse = "This should be application specific";
		return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
	}*/

	/**
	 * <p>
	 * Check this if this required
	 * </p>
	 * @param e
	 * @return
	 */
	@ExceptionHandler(NullPointerException.class)
	public ResponseEntity<ErrorResponse> notFoundException(final NullPointerException e) {
//		return error(e, HttpStatus.NOT_FOUND,"NUll ID" /*e.getId().toString()*/);
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(e), HttpStatus.NOT_FOUND);
	}

	private ResponseEntity<VndErrors> error(final Exception exception, final HttpStatus httpStatus,
			final String logRef) {
		final String message = Optional.of(exception.getMessage()).orElse(exception.getClass().getSimpleName());
		return new ResponseEntity<VndErrors>(new VndErrors(logRef, message), httpStatus);
	}

/*	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<VndErrors> assertionException(final IllegalArgumentException e) {
		return error(e, HttpStatus.NOT_FOUND, e.getLocalizedMessage());
	}*/
}
