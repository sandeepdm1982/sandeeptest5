package net.viralpatel.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import net.viralpatel.spring.dao.CustomerDAO2;
import net.viralpatel.spring.model.Customer;

/**
 * https://www.mkyong.com/spring/spring-jdbctemplate-querying-examples/
 * @author sande
 *
 */
@Component
public class CustomerService 
{
	@Autowired
	private CustomerDAO2 customerDAObj;
	
	
	public List getCustomers() {
		return customerDAObj.getAllCustomers();
	}
	
	public Customer getCustomer(Long customerId) {
		Customer customerObj = customerDAObj.getCustomer(customerId);
		return customerObj;
	}

	public Customer createCustomer(Customer customerObj) {

		customerDAObj.addPerson(customerObj);
		return customerObj;
	}

	public void deleteCustomer(Long id) {
		customerDAObj.deletePerson(id);
	}

	public void updateCustomer(Long customerId,Customer customerObj) {
		customerDAObj.updatePerson(customerObj, customerId);
		return;
	}
	 

}