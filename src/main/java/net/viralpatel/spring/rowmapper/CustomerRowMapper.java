package net.viralpatel.spring.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import net.viralpatel.spring.model.Customer;

/**
 * https://www.mkyong.com/spring/spring-jdbctemplate-querying-examples/
 * @author sande
 *
 */
public class CustomerRowMapper implements RowMapper
{
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		Customer customer = new Customer();
		customer.setId(rs.getLong("id"));
		customer.setFirstName(rs.getString("firstName"));
		customer.setLastName(rs.getString("lastName"));
		customer.setEmail(rs.getString("email"));
		customer.setDateOfBirth(rs.getDate("dateOfBirth"));
		return customer;
	}

}