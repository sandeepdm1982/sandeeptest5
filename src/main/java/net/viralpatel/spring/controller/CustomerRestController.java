package net.viralpatel.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import net.viralpatel.spring.model.Customer;
import net.viralpatel.spring.service.CustomerService;

@RestController
public class CustomerRestController {

	@Autowired
	private CustomerService customerServcieObj;

	@GetMapping("/customers")
	public List getCustomers() throws Exception{
		return customerServcieObj.getCustomers();
	}

	@GetMapping("/customers/{id}")
	public ResponseEntity getCustomer(@PathVariable("id") Long customerId) {

		Customer customer = customerServcieObj.getCustomer(customerId);
		if (customer == null) {
			return new ResponseEntity("No Customer found for ID " + customerId, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity(customer, HttpStatus.OK);
	}

	@PostMapping(value = "/customers")
	public ResponseEntity createCustomer(@RequestBody Customer customer) {
		customerServcieObj.createCustomer(customer);

		return new ResponseEntity(customer, HttpStatus.OK);
	}

	@DeleteMapping("/customers/{id}")
	public ResponseEntity deleteCustomer(@PathVariable Long id) {

		customerServcieObj.deleteCustomer(id);
		// return new ResponseEntity("No Customer found for ID " + id,
		// HttpStatus.NOT_FOUND);
		// }

		return new ResponseEntity(id, HttpStatus.OK);

	}

	@PutMapping("/customers/{customerId}")
	public ResponseEntity updateCustomer(@PathVariable Long customerId, @RequestBody Customer customerObj) {

		// customer = customerDAO.update(id, customer);
		customerServcieObj.updateCustomer(customerId, customerObj);

		if (null == customerObj) {
			return new ResponseEntity("No Customer found for ID " + customerId, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity(customerObj, HttpStatus.OK);
	}

}