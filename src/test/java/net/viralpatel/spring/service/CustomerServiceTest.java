package net.viralpatel.spring.service;

import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import net.viralpatel.spring.config.AppConfig;
import net.viralpatel.spring.model.Customer;

/**
 * https://www.petrikainulainen.net/programming/spring-framework/unit-testing-of-spring-mvc-controllers-rest-api/
 * https://www.mkyong.com/unittest/junit-spring-integration-example/
 * @author sande
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class })
@WebAppConfiguration	
public class CustomerServiceTest {
	@Autowired
	CustomerService customerServiceObj;

	@Test
	    public void testCreateCustomer() {

		  Customer customerObj = new Customer();
		  customerObj.setFirstName("Sandeep Name 4");
		  customerObj.setLastName("DM");
		  customerObj.setEmail("sandeep.dm@gmail.com");
//		  customerObj.setDateOfBirth(dateOfBirth);
	        //assert correct type/impl
		  Customer customer2Obj = customerServiceObj.createCustomer(customerObj);

	        //assert true
		  assertNotEquals(new Long(0), customer2Obj.getId());

	    }

}