package net.viralpatel.spring.controller;

import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import net.viralpatel.spring.model.Customer;
import net.viralpatel.spring.service.CustomerService;
import net.viralpatel.spring.testUtil.TestUtil;

/**
 * http://briansjavablog.blogspot.in/2017/05/rest-endpoint-testing-with-mockmvc.html
 * 
 * @author sande
 *
 */
/*@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class })
@WebAppConfiguration*/
public class CustomerControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext ctx;

	@Mock
	private CustomerService customerServiceObj;

	@InjectMocks
	CustomerRestController customerRestControllerObj;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		// this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
//		customerServiceObj = mock(CustomerService.class);
		mockMvc = MockMvcBuilders.standaloneSetup(customerRestControllerObj)
				/* .setControllerAdvice(new ExceptionHandlers()) */.build();
	}

	@Test
	public void addCustomer() throws Exception {

		Customer customerObj = new Customer();		
		customerObj.setFirstName("Sandeep Name 6");
		customerObj.setLastName("DM");
		customerObj.setEmail("sandeep.dm@gmail.com");
		// customerObj.setDateOfBirth(dateOfBirth);
		
//		when(customerServiceObj.createCustomer(any(Customer.class))).thenReturn(customerObj);		
		MockHttpServletResponse response = mockMvc.perform(post("/customers").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(customerObj))).andReturn().getResponse();
//		assertEquals(200, response.getStatus());
		
		
				// .andExpect(status().isBadRequest())
				/*.andExpect(status()
						.is(200));*//*
									 * .andExpect(content().contentType(TestUtil
									 * .APPLICATION_JSON_UTF8))
									 * .andExpect(jsonPath("$.firstName",
									 * containsInAnyOrder("Sandeep Name 5"))
									 */
		// .andExpect(jsonPath("$.fieldErrors", hasSize(2)))
		// .andExpect(jsonPath("$.fieldErrors[*].path",
		// containsInAnyOrder("firstName", "description")))
		/*
		 * .andExpect(jsonPath("$.fieldErrors[*].message", containsInAnyOrder(
		 * "The maximum length of the description is 500 characters.",
		 * "The maximum length of the title is 100 characters." ))
		 */
		// );
		customerObj.setId(null);
		verify(customerServiceObj,atLeast(1)).createCustomer(argThat(new ObjectEqualityArgumentMatcher<Customer>(customerObj)));
	}

	private class ObjectEqualityArgumentMatcher<T> extends ArgumentMatcher<Customer> {
		Customer thisObject;

	    public ObjectEqualityArgumentMatcher(Customer thisObject) {
	        this.thisObject = thisObject;
	    }

	    @Override
	    public boolean matches(Object argument) {
	    	if(thisObject.getId() == null && ((Customer)argument).getId() == null){
	    		return true;
	    	}
	        return thisObject.equals(argument);
	    }
	}
}